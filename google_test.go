// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package mt

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"
)

//nolint:paralleltest
func TestGoogleV1(t *testing.T) {
	const jsonResponse = `[[["Здравствуйте!","Hello!",null,null,1],[null,null,"Zdravstvuyte!"]],[["междометие",["Здравствуйте!","Привет!","Алло!"],[["Здравствуйте!",["Hello!","How do you do?","How d'ye do?","Good afternoon!","How are you?","Good man!"],null,0.416862],["Привет!",["Hi!","Hello!","Bye!","Hallo!","Hullo!","Ciao!"],null,0.23752081],["Алло!",["Hello!","Hallo!","Hullo!"],null,0.0077553294]],"Hello!",9]],"en",null,null,[["Hello!",null,[["Здравствуйте!",1000,true,false],["Привет!",1000,true,false]],[[0,6]],"Hello!",0,0]],1.0,[],[["en"],null,[1.0],["en"]]]`
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch {
		case r.URL.Path == "/translate_a/single":
			_, _ = w.Write([]byte(jsonResponse))
		default:
			t.Errorf("path:%s rawquery:%s", r.URL.Path, r.URL.RawQuery)
			http.Error(w, "Forbidden", http.StatusForbidden)
		}
	})
	s := httptest.NewServer(handler)
	googleURLGtx = s.URL + "/translate_a/single"
	transl, err := GoogleV1(context.Background(), "en", "ru", "Hello!")
	if err != nil {
		t.Fatal(err)
	}
	const want = "Здравствуйте!"
	if transl != want {
		t.Errorf("want %q, got %q", want, transl)
	}
}

//nolint:paralleltest
func TestGoogleV3(t *testing.T) {
	const jsonResponse = `)]}'

	 [["wrb.fr","MkEWBc","[[\"həˈlō\",null,null,[[[0,[[[null,5]\n]\n,[true]\n]\n]\n]\n,5]\n]\n,[[[null,\"Zdravstvuyte\",null,true,null,[[\"Здравствуйте!\",[\"Здравствуйте\",\"Привет\"]\n]\n]\n]\n]\n,\"ru\",1,\"en\",[\"hello\",\"en\",\"ru\",true]\n]\n,null,[\"hello\",[[[\"惊叹词\",[[\"used as a greeting or to begin a phone conversation.\",\"hello there, Katie!\",true]\n]\n]\n,[\"名词\",[[\"an utterance of “hello”; a greeting.\",\"she was getting polite nods and hellos from people\",true]\n]\n]\n,[\"动词\",[[\"say or shout “hello”; greet someone.\",\"I pressed the phone button and helloed\",true]\n]\n]\n]\n,3]\n,[[[null,\"\\u003cb\\u003ehello\\u003c/b\\u003e there, Katie!\"]\n,[null,\"I said \\u003cb\\u003ehello\\u003c/b\\u003e to him\"]\n,[null,\"\\u003cb\\u003ehello\\u003c/b\\u003e, what's this?\"]\n,[null,\"she refused and, \\u003cb\\u003ehello\\u003c/b\\u003e, I'm her manager!\"]\n,[null,\"\\u003cb\\u003ehello\\u003c/b\\u003e, is anybody in?\"]\n]\n,4,5]\n,null,null,[[[\"动词\",[[\"здороваться\",null,[\"greet\",\"hello\",\"salute\",\"hallo\",\"hullo\",\"bow\"]\n,2,true]\n,[\"звать\",null,[\"call\",\"invite\",\"shout\",\"hail\",\"hallo\",\"hello\"]\n,3,true]\n,[\"окликать\",null,[\"hail\",\"holler\",\"call\",\"challenge\",\"speak\",\"hello\"]\n,3,true]\n]\n,\"ru\",\"en\"]\n,[\"名词\",[[\"приветствие\",null,[\"greeting\",\"welcome\",\"salute\",\"salutation\",\"hello\",\"hail\"]\n,3,true]\n,[\"приветственный возглас\",null,[\"hallo\",\"halloa\",\"hello\",\"viva\"]\n,3,true]\n,[\"возглас удивления\",null,[\"hallo\",\"halloa\",\"hello\"]\n,3,true]\n]\n,\"ru\",\"en\"]\n]\n,6]\n,null,null,\"en\",1]\n]\n",null,null,null,"generic"]
	  ,["di",68]
	   ,["af.httprm",67,"-5484909511130516351",62]
	    ]`
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch {
		case r.URL.Path == "/_/TranslateWebserverUi/data/batchexecute":
			_, _ = w.Write([]byte(jsonResponse))
		default:
			t.Errorf("path:%s rawquery:%s", r.URL.Path, r.URL.RawQuery)
			http.Error(w, "Forbidden", http.StatusForbidden)
		}
	})
	s := httptest.NewServer(handler)
	googlev3APIURL = s.URL
	transl, err := Google(context.Background(), "en", "ru", "Hello!")
	if err != nil {
		t.Fatal(err)
	}
	const want = "Здравствуйте!"
	if transl != want {
		t.Errorf("want %q, got %q", want, transl)
	}
}
