mt [![License](http://img.shields.io/:license-agpl3-blue.svg)](http://www.gnu.org/licenses/agpl-3.0.html) [![Pipeline](https://gitlab.com/opennota/mt/badges/master/pipeline.svg)](https://gitlab.com/opennota/mt/-/commits/master) [![PkgGoDev](https://pkg.go.dev/badge/gitlab.com/opennota/mt)](https://pkg.go.dev/gitlab.com/opennota/mt)
==

Package mt provides machine translation interfaces.

