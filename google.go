// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package mt

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitlab.com/opennota/mt/jsonutil"
	"golang.org/x/time/rate"
)

var (
	googleURLGtx   = "https://translate.googleapis.com/translate_a/single"
	googlev3APIURL = "https://translate.google.com"
	googleLimit    = rate.NewLimiter(rate.Every(time.Second), 1)
	googlev1Limit  = rate.NewLimiter(rate.Every(time.Second), 1)
)

func googleGet(ctx context.Context, url string) (*http.Response, error) {
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Referer", url)
	resp, err := http.DefaultClient.Do(req.WithContext(ctx))
	if err != nil {
		return nil, fmt.Errorf("GET failed: %w", err)
	}
	if resp.StatusCode != 200 {
		resp.Body.Close()
		return nil, fmt.Errorf(http.StatusText(resp.StatusCode))
	}
	return resp, nil
}

func googlePost(ctx context.Context, url string, r io.Reader) (*http.Response, error) {
	req, _ := http.NewRequest("POST", url, r)
	req.Header.Add("Referer", "https://translate.google.com")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded;charset=utf-8")
	resp, err := http.DefaultClient.Do(req.WithContext(ctx))
	if err != nil {
		return nil, fmt.Errorf("POST failed: %w", err)
	}
	if resp.StatusCode != 200 {
		resp.Body.Close()
		return nil, fmt.Errorf(http.StatusText(resp.StatusCode))
	}
	return resp, nil
}

func googleParseResponse(r io.Reader) (string, error) {
	var res []interface{}
	if err := jsonutil.SelectFromReader(&res, r, "[0]"); err != nil {
		return "", fmt.Errorf("unexpected response: %w", err)
	}

	var strs []string
	for _, v := range res {
		v, ok := v.([]interface{})
		if !ok || len(v) == 0 {
			return "", fmt.Errorf("expected a non-empty array")
		}
		if v, ok := v[0].(string); ok {
			strs = append(strs, v)
		}
	}

	return strings.Join(strs, ""), nil
}

func googlev3ParseResponse(r io.Reader) (string, error) {
	if _, err := io.Copy(ioutil.Discard, io.LimitReader(r, 6)); err != nil {
		return "", fmt.Errorf("unexpected response: %w", err)
	}

	var s string
	if err := jsonutil.SelectFromReader(&s, r, "[0][2]"); err != nil {
		return "", fmt.Errorf("unexpected response: %w", err)
	}

	var res7 []interface{}
	if err := jsonutil.SelectFromReader(&res7, strings.NewReader(s), "[1][0][0][5]"); err != nil {
		return "", fmt.Errorf("unexpected response: %w", err)
	}

	strs := make([]string, 0, len(res7))
	for _, v := range res7 {
		v, ok := v.([]interface{})
		if !ok || len(v) == 0 {
			return "", fmt.Errorf("expected a non-empty array")
		}
		s, ok := v[0].(string)
		if !ok {
			return "", fmt.Errorf("expected a string")
		}
		strs = append(strs, s)
	}

	return strings.Join(strs, ""), nil
}

func googlev1(ctx context.Context, sl, tl, text string) (string, error) {
	apiURL := googleURLGtx + fmt.Sprintf("?client=%s&sl=%s&tl=%s&dt=t&q=%s",
		"gtx",
		sl,
		tl,
		url.QueryEscape(text),
	)

	resp, err := googleGet(ctx, apiURL)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	return googleParseResponse(resp.Body)
}

func googlev3(ctx context.Context, sl, tl, text string) (string, error) {
	const rpcID = "MkEWBc"

	param, _ := json.Marshal([]interface{}{
		[]interface{}{
			strings.TrimSpace(text),
			sl,
			tl,
			true,
		},
		[]interface{}{nil},
	})
	rpc, _ := json.Marshal([]interface{}{
		[]interface{}{
			[]interface{}{
				rpcID,
				string(param),
				nil,
				"generic",
			},
		},
	})

	data := url.Values{
		"f.req": {string(rpc)},
	}

	resp, err := googlePost(ctx,
		googlev3APIURL+"/_/TranslateWebserverUi/data/batchexecute",
		strings.NewReader(data.Encode()))
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	return googlev3ParseResponse(resp.Body)
}

// GoogleV1 translates text using Google Translate's old API from the source language sl into the destination language tl.
func GoogleV1(ctx context.Context, sl, tl, text string) (string, error) {
	if err := googlev1Limit.Wait(ctx); err != nil {
		return "", fmt.Errorf("limiter error: %w", err)
	}

	return googlev1(ctx, sl, tl, text)
}

// Google translates text using Google Translate from the source language sl into the destination language tl.
func Google(ctx context.Context, sl, tl, text string) (string, error) {
	if err := googleLimit.Wait(ctx); err != nil {
		return "", fmt.Errorf("limiter error: %w", err)
	}

	return googlev3(ctx, sl, tl, text)
}
