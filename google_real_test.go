// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

//go:build real
// +build real

package mt

import (
	"context"
	"testing"
)

func TestGoogleV1Real(t *testing.T) {
	googleURLGtx = "https://translate.googleapis.com/translate_a/single"
	transl, err := googlev1(context.Background(), "en", "ru", "Hello!")
	if err != nil {
		t.Fatal(err)
	}
	const want = "Здравствуйте!"
	if transl != want {
		t.Errorf("want %q, got %q", want, transl)
	}
}

func TestGoogleV3Real(t *testing.T) {
	googlev3APIURL = "https://translate.google.com"
	transl, err := googlev3(context.Background(), "en", "ru", "Hello!")
	if err != nil {
		t.Fatal(err)
	}
	const want = "Здравствуйте!"
	if transl != want {
		t.Errorf("want %q, got %q", want, transl)
	}
}
