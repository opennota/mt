// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package mt

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"

	"gitlab.com/opennota/mt/jsonutil"
	util "gitlab.com/opennota/mt/yandex-util"
	"golang.org/x/time/rate"
)

var (
	yandexURL       = "https://translate.yandex.com"
	yandexAPIURL    = "https://translate.yandex.net/api/v1/tr.json/translate"
	rSID            = regexp.MustCompile("SID: '(.*?)'")
	yandexLimiter   = rate.NewLimiter(rate.Every(time.Second), 1)
	yandexUserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36"

	yandexM          sync.Mutex
	yandexToken      = ""
	yandexQueryCount = 0

	chunkSize = 600
)

func yandexParseResponse(r io.Reader) (string, error) {
	var text string
	if err := jsonutil.SelectFromReader(&text, r, "text[0]"); err != nil {
		return "", fmt.Errorf("unexpected response: %w", err)
	}
	return text, nil
}

func yaGet(ctx context.Context, url string) (*http.Response, error) {
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Origin", yandexURL)
	req.Header.Add("Referer", yandexURL+"/")
	req.Header.Add("User-Agent", yandexUserAgent)
	resp, err := http.DefaultClient.Do(req.WithContext(ctx))
	if err != nil {
		return nil, fmt.Errorf("GET failed: %w", err)
	}
	return resp, nil
}

func yaPostForm(ctx context.Context, url string, form url.Values) (*http.Response, error) {
	req, _ := http.NewRequest("POST", url, strings.NewReader(form.Encode()))
	req.Header.Add("Origin", yandexURL)
	req.Header.Add("Referer", yandexURL+"/")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("User-Agent", yandexUserAgent)
	resp, err := http.DefaultClient.Do(req.WithContext(ctx))
	if err != nil {
		return nil, fmt.Errorf("POST failed: %w", err)
	}
	if resp.StatusCode != 200 {
		resp.Body.Close()
		return nil, fmt.Errorf(http.StatusText(resp.StatusCode))
	}
	return resp, nil
}

func yandexTransformSID(sid string) string {
	parts := strings.Split(sid, ".")
	for i, p := range parts {
		q := ""
		for _, r := range p {
			q = string(r) + q
		}
		parts[i] = q
	}
	return strings.Join(parts, ".")
}

func yandexGetToken(ctx context.Context) (string, error) {
	resp, err := yaGet(ctx, yandexURL)
	if err != nil {
		return "", fmt.Errorf("yaGet failed: %w", err)
	}
	defer resp.Body.Close()

	var buf bytes.Buffer
	if _, err := buf.ReadFrom(resp.Body); err != nil {
		return "", fmt.Errorf("failed to read page body: %w", err)
	}

	m := rSID.FindStringSubmatch(buf.String())
	if m == nil {
		return "", fmt.Errorf("SID not found")
	}

	return yandexTransformSID(m[1]), nil
}

func yandexTranslate(ctx context.Context, sl, tl, text string, chunkNum int) (string, error) {
	form := url.Values{
		"text":    {text},
		"options": {"4"},
	}
	query := url.Values{
		"id":     {fmt.Sprintf("%s-%d-%d", yandexToken, yandexQueryCount, chunkNum)},
		"lang":   {sl + "-" + tl},
		"srv":    {"tr-text"},
		"reason": {"auto"},
		"format": {"text"},
	}
	resp, err := yaPostForm(ctx, yandexAPIURL+"?"+query.Encode(), form)
	if err != nil {
		return "", fmt.Errorf("yaPostForm failed: %w", err)
	}
	defer resp.Body.Close()

	return yandexParseResponse(resp.Body)
}

// Yandex translates text using Yandex Translate from the source language sl into the destination language tl.
func Yandex(ctx context.Context, sl, tl, text string) (string, error) {
	if err := yandexLimiter.Wait(ctx); err != nil {
		return "", fmt.Errorf("limiter error: %w", err)
	}
	yandexM.Lock()
	defer yandexM.Unlock()

	var err error
	justGotAToken := false
	if yandexToken == "" {
		if yandexToken, err = yandexGetToken(ctx); err != nil {
			return "", fmt.Errorf("failed to get the token: %w", err)
		}
		yandexQueryCount = 0
		justGotAToken = true
	}

	chunks := util.MakeChunks(text, chunkSize)
	var transl []string
outer:
	for {
		for i, c := range chunks {
			t, err := yandexTranslate(ctx, sl, tl, c, i)
			if err != nil {
				if i > 0 || justGotAToken {
					return "", err
				}
				yandexToken, err = yandexGetToken(ctx)
				if err != nil {
					return "", fmt.Errorf("failed to refresh the token: %w", err)
				}
				yandexQueryCount = 0
				justGotAToken = true
				continue outer
			}
			transl = append(transl, t)
		}
		yandexQueryCount++
		break
	}

	return strings.Join(transl, ""), nil
}
