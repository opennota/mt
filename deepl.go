// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package mt

import (
	"context"
	"fmt"
	"io"
	"math"
	"math/rand"
	"net/http"
	"strings"
	"sync/atomic"
	"time"

	"gitlab.com/opennota/mt/jsonutil"
	util "gitlab.com/opennota/mt/yandex-util"
	"golang.org/x/time/rate"
)

const deeplMaxCharCount = 5000

var (
	deeplRequestURL = "https://www2.deepl.com/jsonrpc?method=LMT_handle_jobs"
	deeplLimit      = rate.NewLimiter(rate.Every(time.Second), 1)
	deeplUserAgent  = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36"

	deeplRollingID = int64(1e4 * math.Round(1e4*rand.Float64())) //nolint:gosec
)

func newDeeplRequest(ctx context.Context, sourceLang, targetLang, text string) *http.Request {
	id := atomic.AddInt64(&deeplRollingID, 1)
	timestamp := time.Now().UnixNano() / int64(time.Millisecond)
	n := int64(strings.Count(text, "i") + 1)
	timestamp += n - timestamp%n
	payload := `{"jsonrpc":"2.0","method":"LMT_handle_jobs","params":{"jobs":[{"kind":"default","raw_en_sentence":` + jsonutil.EncodeString(text) + `,"raw_en_context_before":[],"raw_en_context_after":[],"preferred_num_beams":4}],"lang":{"preference":{"weight":{},"default":"default"},"source_lang_computed":"` + strings.ToUpper(sourceLang) + `","target_lang":"` + strings.ToUpper(targetLang) + `"},"priority":1,"commonJobParams":{"formality":null,"browserType":1},"timestamp":` + fmt.Sprint(timestamp) + `},"id":` + fmt.Sprint(id) + `}`
	replaceWith := `hod": "`
	if (id+3)%13 == 0 || (id+5)%29 == 0 {
		replaceWith = `hod" : "`
	}
	payload = strings.Replace(payload, `hod":"`, replaceWith, 1)

	req, _ := http.NewRequestWithContext(ctx, "POST", deeplRequestURL, strings.NewReader(payload))
	req.Host = "www2.deepl.com"
	req.Header.Set("User-Agent", deeplUserAgent)
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Origin", "https://www.deepl.com")
	req.Header.Set("Referer", "https://www.deepl.com/")
	req.Header.Set("TE", "trailers")
	req.Header.Set("Accept", "*/*")
	req.Header.Add("Accept-Language", "en-US,en;q=0.5")
	return req
}

func deeplDo(req *http.Request) (*http.Response, error) {
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("request failed: %w", err)
	}
	if resp.StatusCode != 200 {
		resp.Body.Close()
		return nil, fmt.Errorf(http.StatusText(resp.StatusCode))
	}
	return resp, nil
}

func deeplParseResponse(r io.Reader) (string, error) {
	var text string
	if err := jsonutil.SelectFromReader(&text, r, "result.translations[0].beams[0].postprocessed_sentence"); err != nil {
		return "", fmt.Errorf("unexpected response: %w", err)
	}
	return text, nil
}

// DeepL translates text using DeepL Translator from the source language sl into the destination language tl.
func DeepL(ctx context.Context, sl, tl, text string) (string, error) {
	if err := deeplLimit.Wait(ctx); err != nil {
		return "", fmt.Errorf("limiter error: %w", err)
	}

	chunks := util.MakeChunks(text, deeplMaxCharCount)
	transl := make([]string, 0, len(chunks))
	for _, c := range chunks {
		req := newDeeplRequest(ctx, sl, tl, c)
		resp, err := deeplDo(req)
		if err != nil {
			return "", fmt.Errorf("DeepL request failed: %w", err)
		}
		tr, err := deeplParseResponse(resp.Body)
		if err != nil {
			resp.Body.Close()
			return "", fmt.Errorf("failed to parse DeepL response: %w", err)
		}
		resp.Body.Close()
		transl = append(transl, tr)
	}
	return strings.Join(transl, ""), nil
}
