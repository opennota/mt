// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package util

import (
	"regexp"
	"strings"
	"unicode/utf8"
)

var chunkRegexps = []*regexp.Regexp{
	regexp.MustCompile(`\n+`),
	regexp.MustCompile(`[.!?;\x{0964}](?:\s+|$)`),
	regexp.MustCompile(`[\-\x{2012}-\x{2015}](?:\s+|$)`),
	regexp.MustCompile(`[,:](?:\s+|$)`),
	regexp.MustCompile(`[\x{3002}\x{ff01}\x{ff1f}\x{ff1b}\x{2026}]`),
	regexp.MustCompile(`[\x{ff0c}\x{ff1a}]`),
	regexp.MustCompile(`\s+`),
}

func split(r *regexp.Regexp, s string) []string {
	var ss []string
	for len(s) > 0 {
		loc := r.FindStringIndex(s)
		if loc == nil {
			ss = append(ss, s)
			break
		}
		ss = append(ss, s[0:loc[0]], s[loc[0]:loc[1]])
		s = s[loc[1]:]
	}
	return ss
}

func slice(s string, chunkSize int) string {
	i := 0
	n := 0
	for {
		_, size := utf8.DecodeRuneInString(s[i:])
		if size == 0 || n == chunkSize {
			return s[:i]
		}
		i += size
		n++
	}
}

func truncateText(s string, chunkSize int) string {
	if utf8.RuneCountInString(s) < chunkSize {
		return s
	}

	first := slice(s, chunkSize)
	for _, r := range chunkRegexps {
		if r.MatchString(first) {
			parts := split(r, first)
			return strings.Join(parts[:len(parts)-1], "")
		}
	}
	return s
}

// MakeChunks splits long texts into smaller chunks digestible by Yandex Translate.
func MakeChunks(s string, chunkSize int) []string {
	var chunks []string
	for len(s) > 0 {
		c := truncateText(s, chunkSize)
		chunks = append(chunks, c)
		s = s[len(c):]
	}
	return chunks
}
