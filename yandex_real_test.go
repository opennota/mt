// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

//go:build real
// +build real

package mt

import (
	"context"
	"testing"
)

func TestYandexReal(t *testing.T) {
	yandexURL = "https://translate.yandex.com"
	yandexAPIURL = "https://translate.yandex.net/api/v1/tr.json/translate"
	yandexToken = ""
	yandexQueryCount = 0
	transl, err := Yandex(context.Background(), "en", "ru", "Hello!")
	if err != nil {
		t.Fatal(err)
	}
	const want = "Привет!"
	if transl != want {
		t.Errorf("want %q, got %q", want, transl)
	}
}
