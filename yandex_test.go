// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package mt

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
)

//nolint:paralleltest
func TestYandex(t *testing.T) {
	const jsonResponse = `{"align":["0:6-0:9,0:5-2:6,5:1-8:1"],"code":200,"lang":"en-ru","text":["- Привет!"]}`
	const apiPath = "/api/v1/tr.json/translate"
	const expectedID = "0c1e188e.5f753beb.8f8a959a.74722d74657874-0-0"
	const expectedText = "Hello!"
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch {
		case r.Method == "GET":
			_, _ = w.Write([]byte("SID: 'e881e1c0.beb357f5.a959a8f8.47875647d22747'"))
		case r.Method == "POST" && r.URL.Path == apiPath && r.FormValue("id") == expectedID && r.PostFormValue("text") == expectedText:

			_, _ = w.Write([]byte(jsonResponse))
		default:
			t.Errorf("method:%s path:%s rawquery:%s", r.Method, r.URL.Path, r.URL.RawQuery)
			http.Error(w, "Forbidden", http.StatusForbidden)
		}
	})
	s := httptest.NewServer(handler)
	yandexURL = s.URL
	yandexAPIURL = s.URL + apiPath
	yandexToken = ""
	yandexQueryCount = 0
	transl, err := Yandex(context.Background(), "en", "ru", "Hello!")
	if err != nil {
		t.Fatal(err)
	}
	const want = "- Привет!"
	if transl != want {
		t.Errorf("want %q, got %q", want, transl)
	}
}

//nolint:paralleltest
func TestYandexChunked(t *testing.T) {
	const text = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`
	const respFormat = `{"align":["0:6-0:9,0:5-2:6,5:1-8:1"],"code":200,"lang":"en-ru","text":["%s"]}`
	const apiPath = "/api/v1/tr.json/translate"
	const expectedIDPrefix = "0c1e188e.5f753beb.8f8a959a.74722d74657874-0-"
	expectedTexts := []string{text[:570], text[570:]}
	responses := []string{"Hello, ", "World!"}
	nreq := 0
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch {
		case r.Method == "GET":
			_, _ = w.Write([]byte("SID: 'e881e1c0.beb357f5.a959a8f8.47875647d22747'"))
		case r.Method == "POST" &&
			r.URL.Path == apiPath &&
			strings.HasPrefix(r.FormValue("id"), expectedIDPrefix):

			i, _ := strconv.Atoi(strings.TrimPrefix(r.FormValue("id"), expectedIDPrefix))
			if i != nreq {
				http.Error(w, "Oops!", http.StatusBadRequest)
				return
			}
			nreq++
			if r.PostFormValue("text") != expectedTexts[nreq-1] {
				http.Error(w, "Oops!", http.StatusBadRequest)
				return
			}
			_, _ = w.Write([]byte(fmt.Sprintf(respFormat, responses[nreq-1])))
		default:
			t.Errorf("method:%s path:%s rawquery:%s", r.Method, r.URL.Path, r.URL.RawQuery)
			http.Error(w, "Oops!", http.StatusBadRequest)
		}
	})
	s := httptest.NewServer(handler)
	yandexURL = s.URL
	yandexAPIURL = s.URL + apiPath
	yandexToken = ""
	yandexQueryCount = 0
	transl, err := Yandex(context.Background(), "en", "ru", text)
	if err != nil {
		t.Fatal(err)
	}
	if nreq != 2 {
		t.Errorf("want 2 requests, got %d", nreq)
	}
	const want = "Hello, World!"
	if transl != want {
		t.Errorf("want %q, got %q", want, transl)
	}
}
