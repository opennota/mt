// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package mt

import (
	"strings"
	"testing"
)

func TestDeeplParse(t *testing.T) {
	t.Parallel()
	const jsonText = `{
    "id": 64220006,
    "jsonrpc": "2.0",
    "result": {
        "detectedLanguages": {},
        "source_lang": "EN",
        "source_lang_is_confident": false,
        "target_lang": "RU",
        "translations": [
            {
                "beams": [
                    {
                        "num_symbols": 8,
                        "postprocessed_sentence": "Здравствуй, мир!"
                    },
                    {
                        "num_symbols": 6,
                        "postprocessed_sentence": "Привет, мир!"
                    },
                    {
                        "num_symbols": 6,
                        "postprocessed_sentence": "Здравствуйте, мир!"
                    },
                    {
                        "num_symbols": 8,
                        "postprocessed_sentence": "Приветствую тебя, мир!"
                    }
                ],
                "quality": "normal"
            }
        ]
    }
}`
	got, err := deeplParseResponse(strings.NewReader(jsonText))
	if err != nil {
		t.Fatal(err)
	}
	const want = "Здравствуй, мир!"
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}
}
