// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

//go:build gofuzz

package jsonutil

import (
	"bytes"
	"encoding/json"
)

func trySelect(dst, src interface{}) {
	for _, path := range []string{
		"a",
		"a[1]",
		"a.b",
		"a.b.c",
		"a[0].b",
		"a.b[1]",
		"a[0].b[1]",
		"a[0].b[1].c",
	} {
		Select(dst, src, path)
	}
}

func Fuzz(data []byte) int {
	var m interface{}
	if err := json.NewDecoder(bytes.NewReader(data)).Decode(&m); err != nil {
		return 0
	}
	var a interface{}
	trySelect(&a, m)
	var b int
	trySelect(&b, m)
	var c bool
	trySelect(&c, m)
	var d string
	trySelect(&d, m)
	var e float64
	trySelect(&e, m)
	var f []int
	trySelect(&f, m)
	var g []bool
	trySelect(&g, m)
	var h []string
	trySelect(&h, m)
	var i []float64
	trySelect(&i, m)
	var j map[string]int
	trySelect(&j, m)
	var k map[string]bool
	trySelect(&k, m)
	var l map[string]string
	trySelect(&l, m)
	var n map[string]float64
	trySelect(&n, m)
	var o map[string]interface{}
	trySelect(&o, m)
	var p []interface{}
	trySelect(&p, m)
	return 0
}
