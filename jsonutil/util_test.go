// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jsonutil_test

import (
	"reflect"
	"testing"

	"gitlab.com/opennota/mt/jsonutil"
)

func TestSelect01(t *testing.T) {
	t.Parallel()
	var m interface{} = 1
	var v int
	if err := jsonutil.Select(&v, m, ""); err != nil {
		t.Fatal(err)
	}
	if v != 1 {
		t.Errorf("want %d, got %d", 1, v)
	}
}

func TestSelect02(t *testing.T) {
	t.Parallel()
	m := map[string]interface{}{
		"key": 1,
	}
	var v int
	if err := jsonutil.Select(&v, m, "key"); err != nil {
		t.Fatal(err)
	}
	if v != 1 {
		t.Errorf("want %d, got %d", 1, v)
	}
}

func TestSelect03(t *testing.T) {
	t.Parallel()
	m := []interface{}{0, 1, 2}
	var v int
	if err := jsonutil.Select(&v, m, "[1]"); err != nil {
		t.Fatal(err)
	}
	if v != 1 {
		t.Errorf("want %d, got %d", 1, v)
	}
}

func TestSelect04(t *testing.T) {
	t.Parallel()
	m := map[string]interface{}{
		"key": []int{0, 1, 2},
	}
	var v int
	if err := jsonutil.Select(&v, m, "key[1]"); err != nil {
		t.Fatal(err)
	}
	if v != 1 {
		t.Errorf("want %d, got %d", 1, v)
	}
}

func TestSelect05(t *testing.T) {
	t.Parallel()
	m := map[string]interface{}{
		"k1": []interface{}{
			0,
			map[string]interface{}{
				"k2": []interface{}{3, 4, 1},
			},
			2,
		},
	}
	var v int
	if err := jsonutil.Select(&v, m, "k1[1].k2[2]"); err != nil {
		t.Fatal(err)
	}
	if v != 1 {
		t.Errorf("want %d, got %d", 1, v)
	}
}

func TestSelect06(t *testing.T) {
	t.Parallel()
	m := []interface{}{1, 2, 3}
	var v []int
	if err := jsonutil.Select(&v, m, ""); err != nil {
		t.Fatal(err)
	}

	if want := []int{1, 2, 3}; !reflect.DeepEqual(v, want) {
		t.Errorf("want %v, got %v", want, v)
	}
}

func TestSelect07(t *testing.T) {
	t.Parallel()
	m := map[string]interface{}{
		"1": 1, "2": 2, "3": 3,
	}
	var v map[string]int
	if err := jsonutil.Select(&v, m, ""); err != nil {
		t.Fatal(err)
	}

	if want := map[string]int{"1": 1, "2": 2, "3": 3}; !reflect.DeepEqual(v, want) {
		t.Errorf("want %v, got %v", want, v)
	}
}

func TestSelect08(t *testing.T) {
	t.Parallel()
	m := []interface{}{0, 1, 2}
	var v int
	if err := jsonutil.Select(&v, m, "[4]"); err == nil {
		t.Fatal("want an error, got none")
	}
}

func TestSelect09(t *testing.T) {
	t.Parallel()
	m := []interface{}{0, 1, 2}
	var v int

	if err := jsonutil.Select(&v, m, "[0].oops"); err == nil {
		t.Fatal("want an error, got none")
	}
}

func TestSelect10(t *testing.T) {
	t.Parallel()
	var val map[string]interface{}
	m := map[string]interface{}{
		"key": val,
	}
	var v int

	if err := jsonutil.Select(&v, m, "key.anotherKey"); err == nil {
		t.Fatal("want an error, got none")
	}
}

func TestSelect11(t *testing.T) {
	t.Parallel()
	var val map[string]interface{}
	m := map[string]interface{}{
		"key": val,
	}
	var v int

	if err := jsonutil.Select(&v, m, "key.anotherKey.andAnother"); err == nil {
		t.Fatal("want an error, got none")
	}
}

func TestSelect12(t *testing.T) {
	t.Parallel()
	var val map[string]interface{}
	m := map[string]interface{}{
		"key": val,
	}
	var v int

	if err := jsonutil.Select(&v, m, "key.anotherKey[0]"); err == nil {
		t.Fatal("want an error, got none")
	}
}

func TestSelect13(t *testing.T) {
	t.Parallel()
	var m interface{}
	var v int

	if err := jsonutil.Select(&v, m, "a"); err == nil {
		t.Fatal("want an error, got none")
	}
}

func TestSelect14(t *testing.T) {
	t.Parallel()
	m := map[string]interface{}{
		"key": nil,
	}
	var v int

	if err := jsonutil.Select(&v, m, "key"); err == nil {
		t.Fatal("want an error, got none")
	}
}

func TestSelect15(t *testing.T) {
	t.Parallel()
	m := []interface{}{
		nil,
		[]interface{}{nil, nil, 1},
		nil,
	}
	var v int
	if err := jsonutil.Select(&v, m, "[1][2]"); err != nil {
		t.Fatal(err)
	}
	if v != 1 {
		t.Errorf("want %d, got %d", 1, v)
	}
}

func TestSelect16(t *testing.T) {
	t.Parallel()
	m := map[string]interface{}{
		"key": []interface{}{
			nil,
			[]interface{}{nil, nil, 1},
			nil,
		},
	}
	var v int
	if err := jsonutil.Select(&v, m, "key[1][2]"); err != nil {
		t.Fatal(err)
	}
	if v != 1 {
		t.Errorf("want %d, got %d", 1, v)
	}
}
