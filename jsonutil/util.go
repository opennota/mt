// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jsonutil

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"reflect"
	"strconv"
	"strings"
)

func splitBefore(s, sep string) []string {
	parts := strings.SplitAfter(s, sep)
	result := make([]string, 0, len(parts))
	for i, p := range parts {
		if i < len(parts)-1 {
			p = p[:len(p)-len(sep)]
		}
		if i > 0 {
			p = sep + p
		}
		result = append(result, p)
	}
	return result
}

func EncodeString(s string) string {
	data, _ := json.Marshal(s)
	return string(data)
}

func SelectFromReader(dst interface{}, r io.Reader, path string) error {
	var src interface{}
	if err := json.NewDecoder(r).Decode(&src); err != nil {
		return fmt.Errorf("JSON decoding failed: %w", err)
	}
	return Select(dst, src, path)
}

var errNoValue = errors.New("failed to get a valid value")

func Select(dst, src interface{}, path string) error {
	v := reflect.Indirect(reflect.ValueOf(dst))
	if !v.CanSet() {
		return fmt.Errorf("value of type %v cannot be set", reflect.TypeOf(dst))
	}
	w := reflect.ValueOf(src)

	for _, q := range strings.Split(path, ".") {
		for _, p := range splitBefore(q, "[") {
			if p == "" {
				continue
			}
			if p[0] == '[' {
				index, err := strconv.Atoi(p[1 : len(p)-1])
				if err != nil {
					return fmt.Errorf("invalid index: %s", p[1:len(p)-1])
				}
				if w.Kind() == reflect.Interface {
					w = w.Elem()
				}
				if !w.IsValid() {
					return errNoValue
				}
				if w.Kind() != reflect.Slice {
					return fmt.Errorf("type %v is not a slice", w.Type())
				}
				if index < 0 || index >= w.Len() {
					return fmt.Errorf("invalid index: %d", index)
				}
				w = w.Index(index)
				if !w.IsValid() {
					return errNoValue
				}
			} else {
				if w.Kind() == reflect.Interface {
					w = w.Elem()
				}
				if !w.IsValid() {
					return errNoValue
				}
				if w.Kind() != reflect.Map {
					return fmt.Errorf("type %v is not a map", w.Type())
				}
				w = w.MapIndex(reflect.ValueOf(p))
				if !w.IsValid() {
					return errNoValue
				}
			}
		}
	}

	if w.Kind() == reflect.Interface {
		w = w.Elem()
	}
	if !w.IsValid() {
		return errNoValue
	}

	typeTo := v.Type()
	if w.CanConvert(typeTo) {
		v.Set(w.Convert(typeTo))
		return nil
	}

	if w.Kind() == reflect.Slice && v.Kind() == reflect.Slice {
		sliceType := v.Type()
		elemType := sliceType.Elem()
		s := reflect.MakeSlice(sliceType, w.Len(), w.Len())
		for i := 0; i < w.Len(); i++ {
			x := w.Index(i)
			if x.Kind() == reflect.Interface {
				x = x.Elem()
			}
			if !x.CanConvert(elemType) {
				continue
			}
			s.Index(i).Set(x.Convert(elemType))
		}
		v.Set(s)
		return nil
	}

	if w.Kind() == reflect.Map && v.Kind() == reflect.Map {
		mapType := v.Type()
		keyType := mapType.Key()
		valType := mapType.Elem()
		s := reflect.MakeMapWithSize(mapType, v.Len())
		for _, key := range w.MapKeys() {
			if key.Kind() == reflect.Interface {
				key = key.Elem()
			}
			if !key.CanConvert(keyType) {
				continue
			}
			val := w.MapIndex(key)
			if val.Kind() == reflect.Interface {
				val = val.Elem()
			}
			if !val.CanConvert(valType) {
				continue
			}
			s.SetMapIndex(key.Convert(keyType), val.Convert(valType))
		}
		v.Set(s)
		return nil
	}

	return fmt.Errorf("cannot convert %v to %v", w.Type(), typeTo)
}
